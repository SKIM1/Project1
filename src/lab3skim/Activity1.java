/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3skim;
import java.util.Scanner;

public class Activity1 {

    public static void main(String[] args) {
        double num1;
        double num2;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the first number: ");
        num1 = input.nextDouble();
        
        System.out.println("Please enter the second number: ");
        num2 = input.nextDouble();
        
        double square1 = Math.pow(num1, 2);
        double square2 = Math.pow(num2, 2);
        double sum = square1 + square2;
        double total = square1 - square2;
        
        System.out.println("The square of "+num1+ " is "+square1);
        System.out.println("The square of "+num2+ " is "+square2);
        System.out.println(square1+ "+" +square2+" = "+sum);
        System.out.println(square1+ "-" +square2+" = "+total);
    }
}
