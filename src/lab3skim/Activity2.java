/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3skim;

import javax.swing.JOptionPane;

/**
 *
 * @author Nazatul
 */
public class Activity2 {
    public static void main (String[] args){
        
        String input;
        double radius; 
        
        input = JOptionPane.showInputDialog(null, "Please enter a radius: ");
        radius = Double.parseDouble(input);
        
        double diameter = 2*radius;
        double circumference = 2*3.14159*radius;
        double area = 3.14159* radius*radius;
        
        JOptionPane.showMessageDialog(null,"Diameter: "+diameter+ "\n Circumference: "+circumference+"\n Area: "+area);
    }
}
