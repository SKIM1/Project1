/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3skim;

import java.util.Scanner;
import java.util.*;
/**
 *
 * @author Nazatul
 */
public class Activity5 {
    public static void main (String[] args){
        
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a distance in meter: ");
        double distance = input.nextDouble(); 
        double distance1 = distance / 1000;
        
        System.out.println("Hours: ");
        double hour = input.nextDouble();
        System.out.println("Minutes: ");
        double min = input.nextDouble();
        System.out.println("Seconds: "+"\n");
        double sec = input.nextDouble();
        
        System.out.println("Distance: "+distance+ " Meters");
        System.out.println("Time: "+hour+":"+min+":"+sec+"\n");
        
        // speed = distance / time
        // 1 hour = 3600 secs
        // i min = 60secs
        
        double mps = distance/((hour*3600)+(min*60)+(sec));
        double kmh = distance/ hour + (min/60) + (sec/3600);
        double mph = (distance/1609)/hour + (min/60) + (sec/3600);
        
        System.out.println("M/s: "+mps+ " m/s");
        System.out.println("KM/h: "+kmh+ " km/h");
        System.out.println("M/h: "+mph+ " m/h");
        
    }
}
