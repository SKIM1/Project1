/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3skim;

import java.util.Scanner;

/**
 *
 * @author Nazatul
 */
public class Activity6 {
    public static void main (String[]args){
    
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Value x1: ");
        double x1 = input.nextDouble();
        System.out.println("Enter value x2: ");
        double x2 = input.nextDouble();
        System.out.println("Enter value x3: ");
        double x3 = input.nextDouble();
        
        double mean = (x1+x2+x3)/3;
        double a = Math.pow(x1-mean, 2);
        double b = Math.pow(x2-mean, 2);
        double c = Math.pow(x3-mean, 2);
        double variance = (a+b+c)/3;
        double sd = Math.sqrt(Math.pow(variance, 2));

        System.out.println("x1 = "+x1+"\nx2 = "+x2+" \nx3 = "+x3);
        System.out.println("Mean = "+mean+"\nVariance = "+variance+"\nStandard Deviation = "+sd);
    }
}
